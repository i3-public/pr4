<?php 

# up
# kayo

define( 'FLAG_DOWN',  false ); // false
define( 'FLAG_LOG',   true ); // false
define( 'FLAG_GZIP',  true );  // true
define( 'FLAG_CACHE', false ); // false

define( 'MAIN_PATH',    'http://anselmo.enzoferrari.online:8080' );
define( 'SIGNAL_POINT', 'https://xwork.app' );
define( 'VOD_NODE',     'http://vodn.i3ns.net:8095' );
define( 'PORT_NODE',    'http://port.i3ns.net:1940' );
define( 'S3_NODE',      'http://s3.i3ns.net:2052' );

define( 'STRICT_STREAM_ID',      true );  // true
define( 'STRICT_USER_ID',        true );  // true
define( 'STRICT_MAG_BRUTEFORCE', true );  // true
define( 'STRICT_WEIRD_REQUESTS', false ); // false
