<?php

chdir('/var/www/cron/');
include_once('../inc.php');


$j = 0;

while( 1 ){

	echo ".";

	if(! $json = fgc(SIGNAL_POINT."/api/feed/pr/stream_by_server/") ){
		error_log("ERR: ".__LINE__);

	} else {
		
		$arr = json_decode($json, true);
		$file_s = [];

		foreach( $arr as $server_id => $stream_id_s ){
			foreach( $stream_id_s as $stream_id ){
				list($code, $rest) = code_n_rest($stream_id);
				$file_s[ $code ][ $rest ] = $server_id;			
			}
		}

		$ram_dir = take_care_of_ram_dir('/ram-dir/stream_by_server');
		// echo "care done";
		// exit;
		foreach( $file_s as $code => $arr_of_rest_to_serverId ){
			ksort($arr_of_rest_to_serverId);
			file_put_contents( $ram_dir.'/'.$code.'.php', array_phpfile($arr_of_rest_to_serverId, "server_array") );
			$updated_files[] = $code.'.php';
		}

		chdir($ram_dir);
		foreach( glob('*.php') as $file ){
			if(! in_array($file, $updated_files) ){
				echo "removing $file\n";
				unlink($file);
			}
		}

		
	}

	sleep(9);
	if( ++$j == 6 ) break;

}

