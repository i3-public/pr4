cd /var/www/cron/

renice 12 -p $$

cd /ram-dir
rm -rf cache
mkdir cache cache/data ramvar
chmod -R 0777 *
chown -R www-data:www-data *

cd /ram-dir/ramvar
find . -name "*.txt" -type f -mmin +2 -delete
cd /var/www/cron

for letter in {a..z} ; do
	echo $letter
	find /tmp/cookie/$letter* -type f -mmin +10 -exec rm {} \;
	find /var/lib/php/sessions/sess_$letter* -type f -mmin +10 -exec rm {} \;
done

for letter in {0..9} ; do
	echo $letter
	find /tmp/cookie/$letter* -type f -mmin +10 -exec rm {} \;
	find /var/lib/php/sessions/sess_$letter* -type f -mmin +10 -exec rm {} \;
done

echo "clearCache done."

