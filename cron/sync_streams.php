<?php

chdir('/var/www/cron/');
include_once('../inc.php');


if(! $stream = fgc(SIGNAL_POINT."/api/feed/pr/streams.php") ){
	error_log("ERR: ".__LINE__);

} else {
	
	foreach( explode( ",", $stream ) as $id ){
		$code = substr($id, -2);
		$prefix = substr($id, 0, -2);
		$arr[ $code ][] = $prefix;
	}

	$ram_dir = take_care_of_ram_dir('/ram-dir/stream');
	foreach( $arr as $code => $id_arr ){
		file_safe_put( $ram_dir."/stream_array_".$code.".php", array_phpfile( $id_arr, "stream_array" ) );
	}

	error_log("sync done.");

}


