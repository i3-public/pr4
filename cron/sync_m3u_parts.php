<?php

chdir('/var/www/cron/');
include_once('../inc.php');


# 
# user_bq
sync_api_feed_on_ram('pr4/m3u_parts', 'm3u_parts');





function sync_api_feed_on_ram( $feed, $ram_dir ){

	$api_feed = S3_NODE . "/kron/{$feed}/";

	$file_tar = take_care_of_file('/root/.'.$ram_dir.'.tar');
	$old_md5 = md5_file($file_tar);
	$ram_dir_path = '/ram-dir/'.$ram_dir;

	if(! $new_md5 = fgc($api_feed.".tar.md5") ){
	    echo "cant get md5 from xwork at ".$api_feed.".tar.md5\n";

	} else if( file_exists('/ram-dir/m3u_parts/m3u') and $new_md5 == md5_file($file_tar) ){
	    echo "$ram_dir hit $new_md5\n";

	} else if(! file_put_contents( $file_tar, fgc($api_feed.".tar") ) ){
	    echo "cant put file $file_tar\n";

	} else {

	    echo "$ram_dir miss ".fgc($api_feed.".tar.md5")." != ".$old_md5."\n";

		if( file_exists($ram_dir_path) ){
			shell_exec(
				
				" mkdir {$ram_dir_path}-new ;".
				" chmod -R 0777 {$ram_dir_path}-new ;".
				" chown -R www-data:www-data {$ram_dir_path}-new ;".

				" tar -xf $file_tar -C {$ram_dir_path}-new ;".
				" mv {$ram_dir_path} {$ram_dir_path}-old ;".
				" mv {$ram_dir_path}-new {$ram_dir_path} ;".
				
				" rm -rf {$ram_dir_path}-old "
				
			);

		} else {
			shell_exec(
				
				" mkdir $ram_dir_path ;".
				" chmod -R 0777 {$ram_dir_path} ;".
				" chown -R www-data:www-data {$ram_dir_path} ;".

				" tar -xf $file_tar -C $ram_dir_path "

			);
		}

	}
	
}

