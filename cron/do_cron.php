<?php

chdir('/var/www/cron/');
include_once('../inc.php');

# 
# sync users
if( isset($argv[1]) and $argv[1] == '--cron' ){
	$sec = 10;
	for( $i = 0; $i< (60 / $sec); $i++ ){
		include('sync_users.php');
		echo $i;
		sleep($sec);
	}

} else {
	include_once('sync_users.php');	
}

echo "\n";