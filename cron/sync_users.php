<?php

chdir('/var/www/cron/');
include_once('../inc.php');


if(! $users = fgc(SIGNAL_POINT."/api/feed/etc/users_token/") ){
	error_log("ERR: ".__LINE__);

} else {
	
	$users = json_decode($users);
	
	$ram_dir = take_care_of_ram_dir('/ram-dir/db');
	file_safe_put( $ram_dir."/restreamer_array.php", array_phpfile( $users->restreamer, "restreamer_array" ) );

	foreach( $users->user as $user_token ){
		$code = substr($user_token, -2);
		$prefix = substr($user_token, 0, -2);
		$arr[ $code ][] = $prefix;
	}

	$ram_dir = take_care_of_ram_dir('/ram-dir/user');
	foreach( $arr as $code => $user_arr ){
		file_safe_put( $ram_dir."/user_".$code."_array.php", array_phpfile( $user_arr, "user_array" ) );
	}
	
	error_log("sync done.");

}



