# 
# main port: 8080
# wget --no-cache -qO- https://gitlab.com/i3-public/pr4/-/raw/main/README.txt | bash

apt -y update
apt -y install wget

wget -qO- https://gitlab.com/i3-public/public-text/-/raw/master/preinstall-raw-os | bash
wget -qO- https://gitlab.com/i3-public/conf/-/raw/main/docker/README.txt | bash

docker rm -f pr4
docker rmi pr4-image
docker system prune -a -f

wget -O dockerfile https://gitlab.com/i3-public/conf/-/raw/main/nginx-on-docker/conf/dockerfile
sed -i 's,#INJECT,RUN wget -qO- https://gitlab.com/i3-public/pr4/-/raw/main/conf/patch.sh | sh,g' dockerfile

docker build -t pr4-image -f dockerfile .
rm -rf dockerfile
docker run -t -d --restart unless-stopped --name pr4 --hostname pr4 -p 8080:8080 -p 4022:22 --mount type=tmpfs,destination=/ram-dir pr4-image

