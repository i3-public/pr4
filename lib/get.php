<?php



if( isset($_GET['type']) and in_array($_GET['type'], ['m3u','m3u_plus']) ){

    # plus or not
    $etc = $_GET['type'];

    # prefixed or not
    $uans = str_replace(' ', '', strtolower($_SERVER['HTTP_USER_AGENT']));
    if( str_replace([ 'smarttv', 'tizen', 'bravia', 'tvstore', 'nettv', 'roku', 'aftmm', 'aftss', 'aftka', 'crkey', 'appletv' ], '', $uans) != $uans ){
        // client
    } else if( str_replace([ 'vlc/', 'chrome/', 'safari/', 'firefox/', 'chromium/', 'opera/', 'libmpv/', 'lavf/' ], '', $uans) != $uans ){
        // restreamer
        $etc.= ' prefixed';
        // and if_its($username, $password, 'user')
    } else {
        // unknown
    }

    # user / pass or not
    if( !isset($_GET['username']) or !isset($_GET['password']) )
        do404('nome utente o password non definiti.');

    $username = $_GET['username'];
    $password = $_GET['password'];

    # fill m3u
    $m3u = fill_m3u($req_uri, $username, $password, $etc);

} else {
    $m3u = fill_m3u_from_main($uri, $req_uri);
}


if( FLAG_GZIP == true and strstr($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip') ){
    if( !strstr($uri, '=enigma22_script&') and !strstr($uri, '=dreambox&') ){
        header('Content-Encoding: gzip');
        $m3u = gzencode($m3u, 1);
    }
}

if( strstr($uri, '=enigma22_script&') ){
    $filename = 'iptv.sh';
    $type = 'application/octet-stream';

} else if( strstr($uri, '=dreambox&') ){
    $filename = 'userbouquet.favourites.tv';
    $type = 'application/octet-stream';

} else {
    $filename = $req_domain.".m3u";
    $type = 'audio/mpegurl';
}

header("Content-Type: $type;");
header('Content-Length: '.strlen($m3u));
header("Content-Disposition: attachment;filename=$filename");
header("Pragma: no-cache");
header("Expires: 0");

echo $m3u;



function fill_m3u( $host_n_port, $username, $password, $etc='' ){

    $type_of_content = 
        ( strstr($etc, 'prefixed') ? 'prefixed_' : '').
        'm3u'.
        ( strstr($etc, 'plus') ? '_plus' : '');

    $m3u = "#EXTM3U\n";

    $LINE = "{$host_n_port}/{$username}/{$password}";
    $MOVIE = "{$host_n_port}/movie/{$username}/{$password}";
    $SERIE = "{$host_n_port}/series/{$username}/{$password}";

    // first get the main cats
    $url = PORT_NODE . "/line/cats/?action=get_categories&username={$username}&password={$password}&ignore_subcat=1";
    if(! $cat_s = file_get_contents($url) )
        return false;

    $cat_s = json_decode($cat_s, true);

    foreach( $cat_s as $cat ){
        
        $cat = $cat['category_id'];
        $cat_m3u = file_get_contents("/ram-dir/m3u_parts/{$type_of_content}/{$cat}.m3u");
        // echo $cat_m3u;die;
        if( strstr($cat_m3u, '<LINE>') )
            $cat_m3u = str_replace('<LINE>', $LINE, $cat_m3u);
        
        if( strstr($cat_m3u, '<MOVIE>') )
            $cat_m3u = str_replace('<MOVIE>', $MOVIE, $cat_m3u);
        
        if( strstr($cat_m3u, '<SERIE>') )
            $cat_m3u = str_replace('<SERIE>', $SERIE, $cat_m3u);

        $m3u.= $cat_m3u;
    }

    return $m3u;

}




function fill_m3u_from_main( $uri, $req_uri ){
    
    ini_set('display_errors','On');
    
    if( $m3u = trim( get_content_curl(MAIN_PATH.$uri) ) ){
        $m3u = str_replace(MAIN_PATH, $req_uri, $m3u);
        // $m3u = strlen($m3u);
        return $m3u;
    
    } else {
        do404('Impossibile caricare il file m3u.');
    }

}
