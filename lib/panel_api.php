<?php
include_once('../inc.php');


if( !isset($_GET['username']) or !isset($_GET['password']) )
    do404('nome utente o password non definiti.');

$username = $_GET['username'];
$password = $_GET['password'];


# user info
$data = port( '/line/find/', 'POST', [ 
    'need' => ' id, exp_date, is_trial, created_at, max_connections ',
    'filter' => [
        'username' => $username,
        'password' => $password,
    ],
    'limit' => 1,
    ], true);
$data = (array)$data[0];
extract($data);
# cur
$data = port( '/conx/cur_n_max/', 'POST', [ 'user_id' => $id ], true);
$data = (array) $data;
$active_cons = $data['cur'];
#
$res['user_info'] = [
    "username" => $username,
    "password" => $password,
    "auth" => 1,
    "status" => "Active",
    "exp_date" => strval($exp_date),
    "is_trial" => strval($is_trial),
    "active_cons" => strval($active_cons),
    "created_at" => $created_at,
    "max_connections" => strval($max_connections),
    "allowed_output_formats" => [
        "m3u8",
        "ts",
        "rtmp"
    ]
];


# server info
$res['server_info'] = [
    "url" => 
        explode(':', $_SERVER['HTTP_HOST'])[0],
        // 'iptvtree.net',
    "port" => "8080",
    "https_port" => "8443",
    "server_protocol" => "http"
];

# categories
$cat_s = port( '/cat/find/', 'POST', [ 
    'need' => ' id, category_type, category_name, parent_id ',
    'filter' => [
    	// 'id' => 1
    ],
], true);
$cat_s = (array) $cat_s;
$categories = [];
foreach( $cat_s as $cat ){
    $cat = (array) $cat;
    $categories[ $cat['category_type'] ] [] = [
    	'category_id' => $cat['id'],
    	'category_name' => $cat['category_name'],
    	'parent_id' => $cat['parent_id']
    ];
}
$res['categories'] = $categories;

# channels
$url = PORT_NODE . "/line/cats/?action=get_categories&username={$username}&password={$password}&ignore_subcat=1";

if( $cat_s = file_get_contents($url) ){
    
    $cat_s = json_decode($cat_s, true);
    $json_i = [ 'live' => 0, 'movie' => 0, 'series' => 0 ];
    $available_channels = [];

    foreach( $cat_s as $k => $cat ){

        $cat = $cat['category_id'];
        $cat = file_get_contents("/ram-dir/m3u_parts/json/{$cat}.json");
        $cat = json_decode($cat, true);
        
        foreach( $cat as $stream_id => $stream_detail )
            $cat[ $stream_id ] = ['num'=>++$json_i[ $stream_detail['stream_type'] ] ] + $cat[ $stream_id ];

        $available_channels = $available_channels + $cat;

    }

}
$res['available_channels'] = $available_channels;

logg('panel_api: '.__LINE__.' PORT served', 'uri');
json_die($res);

