<?php

$port_node = PORT_NODE . '/line/cats/';

if( array_key_exists('action', $_REQUEST) ){

	switch( $_REQUEST['action'] ){



		case 'get_series_categories' :
			$uri = $port_node.substr($uri, 15);
			$uri = str_replace('action=get_series_categories', 'action=get_categories&type=series', $uri);
			break;

		
		case 'get_series' :
			
			$res = [];
			$i = 0;
			$filter_s = [];

			if( array_key_exists('category_id', $_REQUEST) )
				$filter_s['category_id'] = intval($_REQUEST['category_id']);
			
			if( array_key_exists('series_id', $_REQUEST) )
				$filter_s['id'] = intval($_REQUEST['series_id']);

			$data = port( '/series/find/', 'POST', [ 
				'need' => ' id, title, category_id, cover, plot, cast, director, genre, releaseDate, last_modified, rating, backdrop_path, youtube_trailer, episode_run_time ',
				'filter' => $filter_s,
			], true);

			foreach( $data as $serie ){

				$serie = (array) $serie;
				extract($serie);

				$name = strstr($title, ": ")
					? substr( strstr($title, ": "), 2 )
					: $title;
				
				$res[] = [
					'num' => ++$i,
					'name' => $name,
					'series_id' => intval($id),
					'cover' => $cover,
					'plot' => $plot,
					'cast' => $cast,
					'director' => $director,
					'genre' => $genre,
					'releaseDate' => $releaseDate,
					'last_modified' => $last_modified,
					'rating' => strval($rating),
					'rating_5based' => round($rating/2, 1),
					'backdrop_path' => ( ($backdrop_path != '""') ? : '') ,
					'youtube_trailer' => $youtube_trailer,
					'episode_run_time' => $episode_run_time,
					'category_id' => strval($category_id)
				];

			}

			json_die($res);

		
		case 'get_series_info' :
			
			if(! $series_id = intval($_REQUEST['series_id']) )
				die('no series_id defined');

			$res['seasons'] = [];

			$json = fgc('http://127.0.0.1:8080/player_api.php?username='.$_REQUEST['username'].'&password='.$_REQUEST['password'].'&action=get_series&series_id='.$series_id);
			$res['info'] = array_except( json_decode($json)[0], ['num', 'series_id'] );
			
			$data = port( '/series/episode_s/', 'GET', [ 'series_id' => $series_id ], true);

			foreach( $data as $e ){
				$res['episodes'][ $e->season_num ][] = [
					'id' => $e->stream_id,
					'episode_num' => intval($e->sort),
					'title' => $e->title,
					"container_extension" => json_decode($e->target_container,true)[0],
					"info" => json_decode($e->info),
					"custom_sid" => "",
					"added" => $e->added,
					'season' => intval($e->season_num),
					'direct_source' => ''
				];
			}

			json_die($res);


		case 'get_vod_categories' :
			$uri = $port_node.substr($uri, 15);
			$uri = str_replace('action=get_vod_categories', 'action=get_categories&type=movie', $uri);
			break;


		case 'get_vod_streams' :
			
			$cat_s = [];

			# single category defined
			if( array_key_exists('category_id', $_REQUEST) ){
				$cat_s[] = intval($_REQUEST['category_id']);
			
			# whole categories
			} else {
				$json = fgc('http://127.0.0.1:8080/player_api.php?username='.$_REQUEST['username'].'&password='.$_REQUEST['password'].'&action=get_vod_categories');
				foreach( json_decode($json) as $c ){
					$cat_s[] = $c->category_id;
				}
			}

			$i = 0;
			$res = [];

			foreach( $cat_s as $cat ){

				$cat = file_get_contents("/ram-dir/m3u_parts/json/{$cat}.json");
				$cat = json_decode($cat, true);

				foreach( $cat as $stream_id => $vod ){
					
					$res[] = [

						'num' => ++$i,
						"name" => $vod['name'],
						"stream_type" => $vod['stream_type'],
						"stream_id" => intval($vod['stream_id']),
						"stream_icon" => $vod['stream_icon'],
						"rating" => strval( round($vod['rating'], 1) ),
						"rating_5based" => $vod['rating_5based'],
						"added" => $vod['added'],
						"category_id" => $vod['category_id'],
						"container_extension" => $vod['container_extension'],
						"custom_sid" => "",
						"direct_source" => ""
						
					];
					
				}

			}
			
			json_die($res);

		
		case 'get_vod_info':

			// http://port.i3ns.net:1940/stream/movie/movie_properties/?movie_id=305959
			$movie_id = intval($_REQUEST['vod_id']);

			$info = port( '/stream/movie/movie_properties/', 'GET', [ 'movie_id'=>$movie_id ], true);
			$info = (array) $info;

			$data = port( '/stream/find/', 'POST', [ 
				'need' => ' added, category_id, target_container ',
				'filter' => [ 'id' => $movie_id ],
				'limit' => 1,
				], true);
			$data = (array)$data[0];
			extract($data);

			$movie_data = [
				"stream_id" => $movie_id,
				"name" => $info['name'],
				"added" => $added,
				"category_id" => $category_id,
				"container_extension" => json_decode($target_container, true)[0],
				"custom_sid" => "",
				"direct_source" => ""
			];

			json_die([
				'info' => $info,
				'movie_data' => $movie_data
			]);

			
		case 'get_live_categories' :
			$uri = $port_node.substr($uri, 15);
			$uri = str_replace('action=get_live_categories', 'action=get_categories&type=live', $uri);
			break;

	
		case 'get_live_streams':

			$res = [];
			$cat_s = [];
			$i = 0;
			
			$filter_s = [ 'type' => 1, 'stream_source'=>['not', '[]'] ];
			if( array_key_exists('category_id', $_REQUEST) )
				$filter_s['category_id'] = intval($_REQUEST['category_id']);
			
			$data = port( '/stream/find/', 'POST', [ 
				'need' => ' id, stream_display_name, type, stream_icon, channel_id, added, category_id ',
				'filter' => $filter_s,
				'order' => ' stream_display_name ',
			], true);

			foreach( $data as $stream ){

				$stream = (array) $stream;
				extract($stream);
				
				$name = strstr($stream_display_name, ": ")
					? substr( strstr($stream_display_name, ": "), 2 )
					: $stream_display_name;
				
				$res[] = [
					"num" => ++$i,
					"name" => $name,
					"stream_type" => 'live',
					"stream_id" => intval($id),
					"stream_icon" => addslashes($stream_icon),
					"epg_channel_id" => ( $channel_id ? : null ),
					"added" => $added,
					"category_id" => $category_id,
					"custom_sid" => null,
					"tv_archive" => 0,
					"direct_source" => "",
					"tv_archive_duration" => 0
				];

			}

			json_die($res);

						
		default: 
			logg(__LINE__, 'uri');
			$uri = MAIN_PATH.$uri;
			break;
			
	}
		
} else {

	if( sizeof($_REQUEST) == 2 and isset($_REQUEST['username']) and isset($_REQUEST['password']) ){
		
		$username = $_REQUEST['username'];
		$password = $_REQUEST['password'];

		# user info
		$data = port( '/line/find/', 'POST', [ 
			'need' => ' id, exp_date, is_trial, created_at, max_connections ',
			'filter' => [
				'username' => $username,
				'password' => $password,
			],
			'limit' => 1,
			], true);
		$data = (array)$data[0];
		extract($data);

		# cur
		$data = port( '/conx/cur_n_max/', 'POST', [ 'user_id' => $id ], true);
		$data = (array) $data;
		$active_cons = $data['cur'];

		json_die([
			'user_info' =>
			[
				"username" => $username,
				"password" => $password,
				"message" => "Welcome to IPTVTREE",
				"auth" => 1,
				"status" => "Active",
				"exp_date" => strval($exp_date),
				"is_trial" => strval($is_trial),
				"active_cons" => strval($active_cons),
				"created_at" => $created_at,
				"max_connections" => strval($max_connections),
				"allowed_output_formats" => [ "m3u8", "ts", "rtmp" ]
			],
			'server_info' => 
			[
				"url" => 
					explode(':', $_SERVER['HTTP_HOST'])[0],
					// 'iptvtree.net',
				"port" => "8080",
				"https_port" => "8443",
				"server_protocol" => "http",
				"rtmp_port" => "8880",
				"timezone" => "Europe/London",
				"timestamp_now" => intval(gmdate('U')),
				"time_now" => gmdate('Y-m-d H:i:s')
			],
		]);
		
	} else {
		$uri = stripslashes($uri);
		$uri = MAIN_PATH.$uri;
	}

	
}

$json = fgc($uri);
file_put_contents('/tmp/'.$_REQUEST['action'], $json);
json_die($json, $skip_encode=true);

