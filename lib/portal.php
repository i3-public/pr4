<?php


if( $_GET['action'] == 'get_categories' and $_GET['type'] == 'series' ){

	$mac = explode('; ', $_SERVER['HTTP_COOKIE'])[0];
	$mac = explode('mac=', $mac)[1];
	// logg('mac: '.$mac, 'uri');
	// $mac = '00%3A1A%3A79%3A99%3A99%3ABF';

	if(! $mac )
		die('no mac defined in cookie'
			// .': '.$_SERVER['HTTP_COOKIE']
		);
	
	$url = PORT_NODE . '/line/cats/?mac='.$mac.'&type=series';
	$data = file_get_contents($url);
	
	// [{"category_id":"112","category_name":"Series \u27be English","parent_id":0}]
	if(! is_json($data) ){
		// die("no json in result: $data");
		die;

	} else {

		$arr_tmp = json_decode($data, true);

		if(! sizeof($arr_tmp) ){
			// die("no data in result: $data");
			die;

		} else {
			foreach( $arr_tmp as $cat ){
				$arr['js'][] = [
					"id" => $cat['category_id'],
					"title" => $cat['category_name'],
					"alias" => $cat['category_name'],
					"censored" => 0
				];
			}
			$data = json_encode($arr);
		}

	}
	

} else {
	$data = get_content_curl(MAIN_PATH.$uri);
}


// if( $_GET['action'] == 'get_series_categories' ){
	$data = preg_replace("/\"name\":\"([A-Z\-])*: /","\"name\":\"",$data);
// }



if( FLAG_GZIP == true and strstr($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip') and 
	( 0
		or str_startsWith($uri, '/portal.php?type=itv&action=get_epg_info&')
		or str_startsWith($uri, '/portal.php?type=itv&action=get_all_channels&')
			// or str_startsWith($uri, '/portal.php?type=itv&action=get_ordered_list&')
	) 
){
	$data = gzencode($data, 1);
	header('Content-Encoding: gzip');
}

header('Cache-Control: no-cache, no-store, max-age=0, must-revalidate');
header('Pragma: no-cache');
header('Content-Type: application/json');
// header('Content-Length: '.strlen($data));

echo $data;


