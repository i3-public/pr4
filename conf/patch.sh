
cd /var
rm -rf www
git clone https://gitlab.com/i3-public/pr4.git
mv pr4 www

cp /var/www/conf/.env.php-sample /var/www/.env.php

mkdir /var/www/log
chmod -R 0777 /var/www/log
chown -R www-data.www-data /var/www/log

wget -O /etc/nginx/sites-available/default https://gitlab.com/i3-public/pr4/-/raw/main/conf/default
service nginx reload
wget -qO- https://gitlab.com/i3-public/pr4/-/raw/main/conf/cron.txt > /var/spool/cron/crontabs/root
wget -qO- https://gitlab.com/i3-public/net-usage/-/raw/master/README.txt | bash -s 8080 skipserver

