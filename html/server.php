<?php

include_once('../inc.php');


# 
block_weird_agent();



# 
# uri variables
if( strstr( $_SERVER['HTTP_HOST'], ':' ) ){
	list( $req_domain, $req_port ) = explode( ':', $_SERVER['HTTP_HOST'] );
	
} else {
	$req_domain = $_SERVER['HTTP_HOST'];
	$req_port = 80;
}

$req_port = $_SERVER['SERVER_PORT'];
$req_uri = "http://".$req_domain.":".$req_port;

$uri = $_SERVER['REQUEST_URI'];

if( $uri == '/favicon.ico' )
	die;

$uri = urldecode($uri);
$uri = str_replace('&amp;', '&', $uri);
$uri = str_replace('//', '/', $uri);
$uri_arr = explode('/', $uri);

logg("uri: ".$uri .( (isset($_POST) and sizeof($_POST)) ? ' POST '.http_build_query($_POST) : '' ), 'uri');



# 
# access control
block_weird_requests($uri);
block_brute_force($uri);
mac_brute_force($uri);


#
# service down
if( FLAG_DOWN == true )
	die('servizio inattivo.');


#
# route
if( $uri == '/c' ){
	header('Location: /c/', true, 301);;
	die;
	
} else if( $uri == '/' ){
    echo "buongiorno principessa.";

} else if( substr($uri, 0, 15) == '/player_api.php' ){
	include_once('../lib/player_api.php');

} else if( substr($uri, 0, 14) == '/panel_api.php' ){
	include_once('../lib/panel_api.php');

} else if( substr($uri, 0, 11) == '/portal.php' ){
	include_once('../lib/portal.php');

} else if( substr($uri, 0, 10) == '/xmltv.php' ){
	header('Content-Type: text/xml');
	header('Content-Disposition: attachment; filename="xmltv.xml"');
	echo get_content_curl( MAIN_PATH .$uri );

} else if( in_array($uri, ['/server/load.php?type=stb&action=handshake&token=&prehash=0&JsHttpRequest=1-xml', '/server/load.php?type=stb&action=get_profile']) ){
	header('Content-Type: application/json');
	header("Pragma: no-cache");
	header("Expires: 0");
	echo get_content_curl( MAIN_PATH .$uri );


} else if( sizeof($uri_arr) == 3 ){ // /moss/alaska33
	list($g, $username, $password) = $uri_arr;
	location($req_uri."/get.php?username=$username&password=$password&type=m3u_plus&output=ts");

	
} else if( substr($uri, 0, 9) == '/get.php?' ){
	include_once('../lib/get.php');
	

} else if( 0
	or ( sizeof($uri_arr) == 4 and is_numeric($uri_arr[3]) ) // /moss/alaska33/99
	or ( sizeof($uri_arr) == 5 and 
		( 0
			or ( in_array( $uri_arr[1], [ 'live', 'movie', 'series' ] ) and in_array( substr(strrchr($uri, '.'), 1) , ['ts', 'm3u8']) )
			or in_array( $uri_arr[1], [ 'movie', 'series' ] )
		)
	)
){

	#
	# fetch elemans
	if( sizeof($uri_arr) == 5 ){
		list( $garbage, $garbage, $username, $password, $stream_id ) = $uri_arr;
		$stream_id = explode('.', $stream_id)[0];

	} else {
		list( $garbage, $username, $password, $stream_id ) = $uri_arr;
	}
	

	# 
	# check stream exists in normal list
	if( STRICT_STREAM_ID == true and (! if_stream_exists_in_db( $stream_id ) ) ){
		do404('il canale o il supporto non esistono.'); // the channel or media does not exists.

	} else {

		#
		# if its a move, redirect it ...
		if( in_array( $uri_arr[1] , [ 'movie', 'series' ] ) ){
			
			if(! if_its( $username, $password, 'user' ) ){
				do404("Impossibile riconoscere l'utente."); // Unable to recognize user.
			
			} else {

				$data = port( '/line/find/', 'POST', [ 
					'need' => ' id ',
					'filter' => [
						'username' => $username,
						'password' => $password,
					],
					'limit' => 1,
				], true);

				$user_id = $data[0]->id;
				
				$hash = hash_generate(10, [ $stream_id, $user_id, remote_ip() ]);
				$extn = substr(strrchr($uri, '.'), 1);

				location( VOD_NODE . "/{$user_id}/{$hash}/{$stream_id}.{$extn}" );

			}

		}
		#




		// new style
		$server_id = get_serverId_by_streamId($stream_id);
		// if server_id in list of smart servers
		// if( in_array($server_id, [ 946, 944 ]) ){
		// if ( $server_id >= 946 ) {
		if( 1 ){
			$url = 'http://'.get_server_addr($server_id).$uri;
			// $url = str_replace([':8080/', ':8070/'], ':9320/', $url);
			location($url);


		// old style
		} else {

			list($full, $code) = get_header_curl( MAIN_PATH . $uri );


			if( $code == 503 ){
				// logg("503 ; ".$uri." ".$full." -- ".$code);
			
			} else if( FLAG_CACHE == true and in_array($code, [ 401, 406, 403 ]) ){
				cache_make( $uri, [ $full, $code ] );
			}
			
			// if( $code == 302 ){
			// 	logg("ts miss; ".$code." ".$stream_id);
			// } else {
			// 	logg("ts miss; ".$code." ".$stream_id." -- ".$uri);				
			// }
			

			if( $code == 302 ){
				
				$patterns = [ 
					'Connection: close',
					'Access-Control-Allow-Origin: *' ];

				foreach( $patterns as $pattern ){
					if( strstr($full, $pattern) ){
						$url = explode( $pattern, $full )[1];
						$url = trim($url, " \n");
					}
				}
				

				// $url = explode('Access-Control-Allow-Origin: *', $full )[1];
				// $url = explode("Server: cloudflare", $url)[0];
				$url = str_replace("Location: ", "", $url);
				$url = trim($url);

				logg("302; ".$url);
				location($url);

			} else {

				// 401 time is over # cache for 2 hours
				// 406 access to bouquete denied # cache for 2 hours
				// 403 provider source is offline # cache for 2 hours
				
				do404('cattiva risposta, full: '. $full. ', code: ' .$code); // cattiva risposta
				// do404('cattiva risposta'); // cattiva risposta

				// echo __LINE__." - ".$code." - ".$its_a;

			}

		} // old style

	}

// other type of requests
} else {

    // logg( date('H:i')."; URI: ".$uri, "mag.log");
    // header('Location: ' . ffmpeg_server_cf.$uri );
    // die('mag service temporary down.');
	
	// /portal.php?type=itv&action=create_link&cmd=ffmpeg%20http://localhost/ch/146316_&series=&forced_storage=0&disable_ad=0&download=0&force_ch_link_check=0&JsHttpRequest=1-xml (GET)
	// if( str_startsWith( $uri, '/portal.php?type=itv&action=create_link&cmd=ffmpeg%20http://localhost/ch/' ) ){
	// 	$old_id = text_between($uri, '/portal.php?type=itv&action=create_link&cmd=ffmpeg%20http://localhost/ch/', '_');

	// // /portal.php?type=itv&action=get_short_epg&ch_id=146316&size=10&JsHttpRequest=1-xml (GET)
	// } else if( str_startsWith( $uri, '/portal.php?type=itv&action=get_short_epg&ch_id=' ) ){
	// 	$old_id = text_between($uri, '/portal.php?type=itv&action=get_short_epg&ch_id=', '&');
		
	// // /portal.php?type=itv&action=set_last_id&id=146316&JsHttpRequest=1-xml (GET)
	// } else if( str_startsWith( $uri, '/portal.php?type=itv&action=set_last_id&id=' ) ){
	// 	$old_id = text_between($uri, '/portal.php?type=itv&action=set_last_id&id=', '&');

	// // /portal.php?type=stb&action=log&real_action=play&param=[object%20Object]&content_id=0&tmp_type=1&id=146316&cmd=ffmpeg%20http://iptvtree.net:8080/7TmIqQxtI6/GEJxKNn7Gb/146316?play_token=02rKUr8PMf&JsHttpRequest=1-xml (GET)
	// } else if( str_startsWith( $uri, '/portal.php?type=stb&action=log&real_action=play&param=[object%20Object]&content_id=0&tmp_type=1&id=' ) ){
	// 	$old_id = text_between($uri, '/portal.php?type=stb&action=log&real_action=play&param=[object%20Object]&content_id=0&tmp_type=1&id=', '&');
	// }

	$data = get_content_curl( MAIN_PATH .$uri );
	
	if( FLAG_GZIP == true and strstr($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip') and 
		( 0
			or str_startsWith($uri, '/portal.php?type=itv&action=get_epg_info&')
			or str_startsWith($uri, '/portal.php?type=itv&action=get_all_channels&')
 			// or str_startsWith($uri, '/portal.php?type=itv&action=get_ordered_list&')
			or text_match($uri, '/panel_api.php?username=(*)&password=(*)')
		) 
	){
		$data = gzencode($data, 1);
		header('Content-Encoding: gzip');
	}
    
	header('Cache-Control: no-cache, no-store, max-age=0, must-revalidate');
	header('Pragma: no-cache');
	header('Content-Type: application/json');
	// header('Content-Length: '.strlen($data));
	
	echo $data;
	
}


