<?php


$env = '/var/www/.env.php';
if( file_exists($env) ){
	include_once($env);
} else {
	die("no .env.php defined");
}


if( defined('RESTRICTED_IPS') and !is_null(RESTRICTED_IPS) )
	if(! in_array($_SERVER['REMOTE_ADDR'], explode(' ', RESTRICTED_IPS) ) )
		die('access restricted.');


include_once('config.'.CONF_NAME.'.php');

function numerize($n){

	$n = explode(".", $n);

	$str = '';
	foreach( $n as $ni ){
		if( strlen($ni) < 3 ){
			$ni = str_repeat( '0', 3-strlen($ni) ) . $ni;
		}
		$str.= $ni;
	}

	return intval($str);
}




if(! function_exists('is_json') ){
	
	function is_json($string) {
		return ((is_string($string) &&
			(is_object(json_decode($string)) ||
			is_array(json_decode($string))))) ? true : false;
	}

}




function HandleHeaderLine( $curl, $header_line ) {
	echo $header_line; // or do whatever
	return strlen($header_line);
}




function get_header_curl( $url, $timeout=60 ){

	ob_start();
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT'] );
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_HEADERFUNCTION, "HandleHeaderLine");
	curl_setopt($ch, CURLOPT_HTTPHEADER, [
	    'User-Agent: '.$_SERVER['HTTP_USER_AGENT'],
		"CF-Connecting-IP: ".$_SERVER['REMOTE_ADDR'],
	]);

	if( array_key_exists('HTTP_REFERER', $_SERVER ) )
		curl_setopt($ch, CURLOPT_REFERER, str_replace($_SERVER['SERVER_ADDR'], 'iptvtree.net', $_SERVER['HTTP_REFERER']) );

	# curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0); 
	curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);

	$body = curl_exec($ch);

	$r = ob_get_contents();
	ob_end_clean();


	$code = explode("\n", $r)[0];
	$code = trim($code);
	$code = explode(" ", $code)[1];
	$code = trim($code);

	return [ $r, $code ];

}




function get_content_curl( $url, $post=null, $timeout=60 ){

	$url = urlencode_params($url);

	// if(! file_exists('/tmp/kk') )
	// 	@mkdir('/tmp/kk');
	// $md5_tmp = "/tmp/kk/".md5($url);
	// logg( $url." -> ".md5($url), 'uri');
	// file_put_contents($md5_tmp, $url." ...");

	if( !$post and $_POST )
		$post = $_POST;

	ob_start();
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HTTP_VERSION, 1.1);
	curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT'] );
	$headers = [
	    'Host: '.$_SERVER['HTTP_HOST'],
	    'User-Agent: '.$_SERVER['HTTP_USER_AGENT'],
		'CF-Connecting-IP: '.$_SERVER['REMOTE_ADDR'],
	];

	if( isset($_SERVER['HTTP_ACCEPT_ENCODING']) )
		$headers[] = 'Accept-Encoding: '.$_SERVER['HTTP_ACCEPT_ENCODING'];
	
	if( isset($_SERVER['HTTP_ACCEPT_LANGUAGE']) )
		$headers[] = 'Accept-Language: '.$_SERVER['HTTP_ACCEPT_LANGUAGE'];
	
	if( isset($_SERVER['HTTP_COOKIE']) )
		$headers[] = 'Cookie: '.$_SERVER['HTTP_COOKIE'];
	
	if( isset($_SERVER['HTTP_ACCEPT']) )
		$headers[] = 'Accept: '.$_SERVER['HTTP_ACCEPT'];

	if( isset($_SERVER['HTTP_CONNECTION']) )
		$headers[] = 'Connection: '.$_SERVER['HTTP_CONNECTION'];

	if( array_key_exists('HTTP_REFERER', $_SERVER ) )
		curl_setopt($ch, CURLOPT_REFERER, str_replace($_SERVER['HTTP_HOST'], 'iptvtree.net:8080', $_SERVER['HTTP_REFERER']) );

	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	if( $post ){
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
	}
	curl_setopt($ch, CURLOPT_HEADERFUNCTION, "HandleHeaderLine");
	
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0); 
	curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);

	if( strstr($url, 'client_area') ){
		curl_setopt ($ch, CURLOPT_COOKIEJAR, '/tmp/cookie/'.session_id() ); 
		curl_setopt ($ch, CURLOPT_COOKIEFILE, '/tmp/cookie/'.session_id() ); 
	}

	$body = curl_exec($ch);

	$r = ob_get_contents();
	ob_end_clean();

	if( stristr($r, 'gzip') )
		$body = gzdecode($body);

	# 8080\/moss\/0D3plRqm1z\/5378
	// if( strstr($body, 'iptvtree') ){
	// 	logg('found iptvtree', 'uri');
	// 	$body = str_replace('iptvtree.net', '46.229.243.199', $body);
	// 	if( strstr($body, '8080\/moss\/') ){
	// 		$pass = explode('8080\/moss\/', $body)[1];
	// 		$pass = explode('\/', $pass)[0];
	// 		$body = str_replace($pass, 'alaska63', $body);
	// 		logg("pass replacement $pass -> alaska33", 'uri');
	// 	} else {
	// 		logg('not found pass', 'uri');
	// 	}
	// }

	// @file_put_contents($md5_tmp, $body);
	// logg(__LINE__.": ".$r."\nbody: ".$body , 'res');
	// logg("r ".strlen($r).", body ".strlen($body)." ".$md5_tmp , 'uri');

	$code = explode("\n", $r)[0];
	$code = trim($code);
	$code = explode(" ", $code)[1];
	$code = trim($code);

	if( in_array( $code, [301, 302] ) ){
		$url = explode('Location: ', $r)[1];
		$url = explode('Connection:', $url)[0];
		$url = trim($url, " \r\n");
		$main_ip = explode(':', substr(MAIN_PATH, 7) )[0];
		$url = str_replace($main_ip, explode( ':', $_SERVER['HTTP_HOST'] )[0] , $url);
		
		location($url);
	
	} else {
		return $body;
	}

}



function urlencode_params( $url ){

	if( strstr($url, '?') ){
		
		list($url, $qs) = explode('?', $url);
		parse_str($qs, $qs_arr);

		$param_arr = [];
		foreach( $qs_arr as $name => $value )
			$param_arr[] = $name."=".rawurlencode($value);
		
		$url.= "?" . implode('&', $param_arr);

	}

	return $url;

}



function block_brute_force($uri){

	if( STRICT_USER_ID != true ) return true;

	// return true;
	$uri_arr = explode('/', $uri);

	if( str_replace( ['&', '?', '='], '', $uri) == $uri and sizeof($uri_arr) == 4 and is_numeric($uri_arr[3]) ){
		$uri_tmp = explode("/", $uri);
		$user = $uri_tmp[1];
		$pass = $uri_tmp[2];

	} else if( sizeof($uri_arr) == 5 and ( 0 or substr($uri, 0, 6) == '/live/' or substr($uri, 0, 7) == '/movie/' or substr($uri, 0, 8) == '/series/' ) ){
		$user = $uri_arr[2];
		$pass = $uri_arr[3];

	} else if( sizeof($uri_arr) == 4 and is_numeric($uri_arr[3]) ){ // NULL/user/pass/81238
		$user = $uri_arr[1];
		$pass = $uri_arr[2];

	} else if( strstr($uri, 'username=') ){
		$user = explode('username=', $uri)[1];
		$user = explode('&', $user)[0];
		$pass = explode('password=', $uri)[1];
		$pass = explode('&', $pass)[0];
		
	} else {
		return true;
	}


		
	if(! if_its( $user, $pass ) ){
	
		logg('BANNED USER: '.$user.'/'.$pass.' -- '.$_SERVER['HTTP_HOST']." ; ".$_SERVER['HTTP_USER_AGENT'], "banned.log");
		// echo $user." banned<br>";
		// echo "hold on for 1 minute";
		do404('l\'utente non esiste.');

	}

}




// user or restreamer only
function if_its( $user, $pass, $db=null ){

	if(! $db ){
		if( if_its( $user, $pass, 'restreamer' ) ){
			return true;
		} else if( if_its( $user, $pass, 'user' ) ){
			return true;
		} else {
			return false;
		}
	}

	return if_guy_exists_in_db( pass_token( $user, $pass ), $db );

}




function pass_token( $user, $pass ){
	return substr( md5( $user . '/' . $pass ), 10, 6 );
}



function if_guy_exists_in_db( $token, $db ){

	if( $db == 'restreamer' ){
		include("/ram-dir/db/restreamer_array.php");
		$arr = "restreamer_array";
	
	} else {
		$code = substr($token, -2);
		$token = substr($token, 0, -2);
		include("/ram-dir/user/user_".$code."_array.php");
		$arr = "user_array";
	}
	
	if( in_array($token, $$arr) ){
		return true;
	
	} else {
		return false;
	}

}



function if_stream_exists_in_db( $id ){

	$code = substr($id, -2);
	$prefix = substr($id, 0, -2);

	include("/ram-dir/stream/stream_array_".$code.".php");

	if( in_array($prefix, $stream_array) ){
		return true;
	
	} else {
		return false;
	}

}



function file_safe_put( $file, $data ){

	file_put_contents( $file."_tmp", $data );
	
	if( file_exists($file) ){
		rename( $file, $file."_remove" );
		rename( $file."_tmp", $file );
		unlink( $file."_remove" );

	} else {
		rename( $file."_tmp", $file );		
	}

}




function cache_path( $uri ){

	$md5 = md5($uri);
	$cache_file = $md5.".txt";

	$d1 = substr($md5,-6,-4);
	$d2 = substr($md5,-4,-2);
	$d3 = substr($md5,-2);

	$cache_path = "/ram-dir/cache/data/$d1/$d2/$d3/$cache_file";
	
	if(! file_exists("/ram-dir/cache/data/$d1/$d2/$d3") ){
		// if(! file_exists("/ram-dir/cache/data/$d1/$d2") ){
		// 	if(! file_exists("/ram-dir/cache/data/$d1") ){
		// 		take_care_of_ram_dir("/ram-dir/cache/data/$d1");
		// 	}
		// 	take_care_of_ram_dir("/ram-dir/cache/data/$d1/$d2");
		// }
		take_care_of_ram_dir("/ram-dir/cache/data/$d1/$d2/$d3");
	}

	return $cache_path;
}




function cache_make( $uri, $content ){

	if( is_array($content) ){
		$content = json_encode($content);
	}

	$cache_path = cache_path($uri);
	file_put_contents( $cache_path , $content );

	return true;
}




function cache_hit( $uri ){

	$cache_path = cache_path($uri);
	
	if( file_exists($cache_path) ){
		$content = file_get_contents($cache_path);
		if( is_json($content) ){
			$content = json_decode($content);
		}
		return $content;

	} else {
		return false;
	}

}





function do404( $message = '' ){

logg($message, 'do404');

header('Content-type: text/html; charset=utf-8');
header('HTTP/1.0 404 Not Found');

?><!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
<html>
    
    <head>
    <title>404 User Not Found<?= $message ? ", ".$message : '' ?></title>
    
    <style>
        * {
            font-family: Monospace, sans-serif;
            padding: 0;
            margin: 0;
        }
        body {
            background-color: rgba(0,0,0, 0.05);
        }
        center {
            margin-top:50px;
        }
    </style>
    
</head>
<body>
    <center>
        <h1>Non trovato.</h1>
        <h3><?= $message ?></h3>
    </center>
</body>
</html><?php

die;

}




function array_phpfile( $array, $arrayName, $memo="" ){
	
	$k = 0;
	$phpfile = "<?php\r\n\r\n".($memo ? "# ".$memo."\r\n\r\n":"")."\$".$arrayName." = [\n\t";
	
	if( is_associative_array($array) ){

		$the_key_type_string = false;
		$the_val_type_string = false;

		foreach( $array as $item_key => $item_value ){
			if( $the_key_type_string and $the_val_type_string ) break;
			if( !$the_key_type_string and !is_numeric($item_key) ){
				$the_key_type_string = true;
			}
			if( !$the_val_type_string and !is_numeric($item_value) ){
				$the_val_type_string = true;
			}
		}

		foreach( $array as $item_key => $item_value ){
			$phpfile.= " ".     ( $the_key_type_string ? "\"$item_key\""   : $item_key   ).
					   "\t=>\t".( $the_val_type_string ? "\"$item_value\"" : $item_value ).
					   " ,\r\n\t";
		}

	} else {

		$item_type_string = false;

		foreach( $array as $item ){
			if(! is_numeric($item) ){
				$item_type_string = true;
				break;
			}
		}
		
		foreach( $array as $item ){
			
			$phpfile.= $item_type_string ? "\"$item\", " : "$item, ";				

			if(! (++$k%8) ){
				$phpfile.= "\r\n\t";
			}

		}

	}

	$phpfile.= "\n];\r\n\r\n";

	return $phpfile;
	
}




function logg( $c, $file="logg.log", $force=false ){

	$file = "/tmp/log/".$file;

	if( 
		FLAG_LOG == true
		 // or $force
	){

		$c = remote_ip().str_repeat(" ", 16 - strlen(remote_ip()) )."; ".$c."\n";

		if( $fp = @fopen($file, 'a+') ){
			fwrite($fp, $c);
			fclose($fp);
		}
		
	}
	
}



function is_associative_array( $arr ){

    if( array() === $arr )return false;
    return array_keys($arr) !== range(0, count($arr) - 1);

}



function str_startsWith( $string, $startString ){ 
    
	$string = urldecode($string);
	$startString = urldecode($startString);

    $len = strlen($startString);

    return (substr($string, 0, $len) == $startString);

}


function text_between( $text, $start, $end ){

	$text = urldecode($text);
	$start = urldecode($start);
	$end = urldecode($end);

    $text = explode($start, $text)[1];
    $text = explode($end, $text)[0];

    return $text;

}


# 1 Aug 2020
function fgc( $url ){
	
	return @file_get_contents( $url , false, stream_context_create( [ "ssl"=> [ "verify_peer"=>false, "verify_peer_name"=>false ] ] ) );

}



# 2020-11-10
function hash_generate( $length = 10, $text='' ){

	if(! $text ){
		$text = date('U');
		$text.= rand(0 , 999999);
	
	} else if( is_array($text) ){
		$text = implode("%$%", $text);
	}

	$text = md5($text);
	$text.= sha1($text);
	$text.= md5($text);

	$text = substr($text, 8, $length);

	return $text;

}



#
# 2020-11-27
function remote_ip(){

	if( isset($_SERVER['HTTP_CF_CONNECTING_IP']) )
		return $_SERVER['HTTP_CF_CONNECTING_IP'];

	if( isset($_SERVER['HTTP_TUNNEL']) )
		return $_SERVER['HTTP_TUNNEL'];
	
	return $_SERVER['REMOTE_ADDR'];
	
}



function location( $url, $code=302 ){

	// if( $_SERVER['HTTP_TUNNEL'] ){
		// echo "redirect $url";

	// } else {
		header( 'Location: ' . $url );
	// }

	die();

}





#
# eggbox 
# v-1.0
#

function eggbox( $name, $value=null, $expire=null ){

	# 
	# if ramdir exists
	if(! file_exists('/ram-dir') ){
		logg('need ram-dir', 'force', true);
		die;
	}


	#
	# init
	$eggbox = '/ram-dir/eggbox/';
	take_care_of_ram_dir($eggbox);
	$json = $eggbox.$name.'.json';
	$sema = $eggbox.$name.'.sema';


	#
	# sema open
	$ms = rand(30, 150) * 1000;
	while( true ){
		if( file_exists($sema) ){
			usleep($ms);
		} else {
			file_put_contents($sema, 'bla');
			break;
		}
	}


	# 
	# load old list
	if(! file_exists($json) ){
		$list = [];
	
	} else if(! $json_content = file_get_contents($json) ){
		$list = [];
		
	} else if(! is_json($json_content) ){
		$list = [];
		
	} else if(! $list = json_decode($json_content , true) ){
		$list = [];
		
	} else if( sizeof($list) ){
		foreach( $list as $i => $item ){
			if( isset($item['expire_date']) and $item['expire_date'] < date('U') ){
				unset($list[$i]);
			}
		}
	}


	#
	# put
	if( $value ){

		$item['value'] = $value;

		if( $expire ){
			$item['expire_date'] = date('U') + $expire;
		}

		$list[] = $item;
	}


	#
	# update the json
	file_put_contents( $json, json_encode($list) );



	#
	# sema close
	unlink($sema);


	# 
	# set the values and return
	$value_s = [];
	foreach( $list as $item ){
		$value_s[] = $item['value'];
	}

	return $value_s;


}


function ramvar($name, $expire=null, $value=null){

	#
	# if ramdir exists
	if(! file_exists('/ram-dir') ){
		echo 'need ram-dir';
		die();
	}

	#
	# init
	$dir = take_care_of_ram_dir('/ram-dir/ramvar');
	$file = $dir.'/'.md5($name).'.txt';


	#
	# forget old, if its late
	if( $expire and file_exists($file) and filemtime($file) + $expire < date('U') ){
		unlink($file);
	}

	#
	# set the value
	if( $value ){
		file_put_contents($file, $value);
	
	#
	# retrive the value
	} else {

		#
		# retrive
		return file_exists($file) ? file_get_contents($file) : null;

	}

}


function ramvar_inc( $name, $expire=null ){

	$value = intval(ramvar($name, $expire)) + 1;
	ramvar($name, $expire, $value);

}


function mac_brute_force( $url ){

	if( STRICT_MAG_BRUTEFORCE !== true )
		return true;

	#
	# non mag request
	if(! strstr($url, '&mac=') and !$_SERVER['HTTP_COOKIE'] )
		return true;

	#
	# vars
	$limit_count_of_visit = 5;
	$limit_visit_duration = 60;
	$limit_blacklist_time = 600;

	#
	# check for blacklist
	if( in_array( remote_ip(), eggbox(__FUNCTION__) ) )
		do404('accesso limitato.');

	#
	# if its a mac request
	if( strstr($url, '&mac=') ){

		#
		# count the requests
		$varName = __FUNCTION__.'_request_counter_'.remote_ip();
		ramvar_inc($varName, $limit_visit_duration);

		if( ramvar($varName, $limit_visit_duration) >= $limit_count_of_visit ){
			eggbox(__FUNCTION__, remote_ip(), $limit_blacklist_time);
			do404('accesso limitato.');
		}


	#
	# if there is a mag cookie
	} else if( $cookie_code = $_SERVER['HTTP_COOKIE'] ){

		$mac = urldecode( text_between($cookie_code, 'mac=', ';') );

		$varName = __FUNCTION__.'_mac_counter_'.remote_ip();

		if( $json = ramvar($varName, $limit_visit_duration) ){
			$list = json_decode($json, true);
		} else {
			$list = [];
		}

		if(! in_array($mac, $list) ){
			$list[] = $mac;
			ramvar($varName, $limit_visit_duration, json_encode($list) );
		}

		if( sizeof($list) >= $limit_count_of_visit ){
			eggbox(__FUNCTION__, remote_ip(), $limit_blacklist_time);
			do404('accesso limitato.');
		}

	}
	
}




function block_weird_requests($uri){


	if( STRICT_WEIRD_REQUESTS != true )
		return true;


	if( strstr($uri, '?play_token=') and is_alphanum($_GET['play_token']) and sizeof($_GET)==1 ){
		$uri_arr = explode('/', substr(explode('?', $uri)[0],1) );

	} else if( !$_POST and !strstr($uri, '?') and $uri != '/' ){
		$uri_arr = explode('/', substr($uri,1) );

	} else {
		$uri_arr = [];
	}


	# 
	# publics
	if( $uri == '/c' or str_startsWith($uri, '/c/') ){
		return true;

	} else if( $uri == '/' ){
		return true;


	# 
	# m3u
	} else if( sizeof($uri_arr) == 2 and is_alphanum($uri_arr[0], '\-\_') and is_alphanum($uri_arr[1], '\.') ){
		return true;

	} else if( sizeof($uri_arr) == 3 and is_alphanum($uri_arr[0], '\-\_') and is_alphanum($uri_arr[1], '\.') and is_numeric($uri_arr[2]) ){
		return true;

	} else if( sizeof($uri_arr) == 4 and in_array($uri_arr[0], ['live', 'movie', 'series']) and is_alphanum($uri_arr[1], '\-\_') and is_alphanum($uri_arr[2], '\.') and is_alphanum($uri_arr[3], '\.') ){
		return true;
	
	# get.php
	} else if( str_startsWith($uri, '/get.php?') and array_keys_in($_GET,['username','password','type','output']) and is_alphanum($_GET, '\-\_\.') ){	
		return true;


	} else if( in_array($uri, ['/server/load.php?type=stb&action=handshake&token=&prehash=0&JsHttpRequest=1-xml', '/server/load.php?type=stb&action=get_profile']) ){
		return true;


	# 
	# xtream code api
	} else if( ( str_startsWith($uri, '/player_api.php?') or str_startsWith($uri, '/panel_api.php?') ) and array_keys_in($_GET,['username','password','action','series_id','category_id','stream_id','vod_id']) and is_alphanum($_GET, '\-\_\.') ){
		return true;

	# xmltv
	} else if( str_startsWith($uri, '/xmltv.php?') and array_keys_in($_GET,['username','password','type','output']) and is_alphanum($_GET, '\-\.') ){
		return true;

	# smart on android
	} else if( in_array($uri, ['/player_api.php','/panel_api.php']) and $_POST and array_keys_in($_POST,['username','password','action']) and is_alphanum(array_values($_POST), '\-\.\_') ){
		return true;

	# 
	# MAG
	} else if( str_startsWith($uri, '/portal.php?') ){
		
		if( array_keys_in($_GET,['type','action','JsHttpRequest','token','prehash','gmode','fav','force_ch_link_check','period','cur_play_type','event_active_id','init','real_action','param','content_id','tmp_type','video_mode','fav_itv_on','genre','sortby','hd','p','ch_id','size','play_token','id','from_ch_id','category','cat_alias','not_ended','row','episode_id','season_id','movie_id','download','disable_ad','forced_storage','series','cmd','vol','date']) and is_alphanum(array_values($_GET), '\-\_\=') ){
			return true;

		# /portal.php?type=stb&action=get_profile&hd=1&ver=ImageDescription: Factory image; ImageDate: ; PORTAL version: 5.3.0; API Version: JS API version: 343; STB API version: 0; Player Engine version: 0x 0&num_banks=1&sn=102018N028149&stb_type=MAG322&client_type=STB&image_version=0&video_out=hdmi&device_id=60BAAAAAE65824F661DF43B27A9CAB2A500F71F63542607065DAEA7915EEEC71&device_id2=&signature=&auth_second_step=0&hw_version=11.12-B0ZZ-00&not_valid_token=0&metrics={"mac":"00:1A:79:5D:CF:DC","sn":"102018N028149","model":"MAG322","type":"STB","uid":"","random":""}&hw_version_2=48120f05f23c7710549eef58ef82a9a2680e1f5b&timestamp=1622817477&api_signature=238&prehash=false&JsHttpRequest=1-xml
		} else if( array_keys_in($_GET,[ 'type', 'action', 'hd', 'ver', 'num_banks', 'sn', 'stb_type', 'client_type', 'image_version', 'video_out', 'device_id', 'device_id2', 'signature', 'auth_second_step', 'hw_version', 'not_valid_token', 'metrics', 'hw_version_2', 'timestamp', 'api_signature', 'prehash', 'JsHttpRequest' ]) and is_alphanum(array_values($_GET), '\-\.\_\"\:\;\,\{\}\ ') ){
			return true;

		# /portal.php?type=itv&action=create_link&cmd=ffmpeg http://localhost/ch/145013_&series=&forced_storage=0&disable_ad=0&download=0&force_ch_link_check=0&JsHttpRequest=1-xml
		} else if( array_keys_in($_GET,['type','action','cmd','series','forced_storage','disable_ad','download','force_ch_link_check','JsHttpRequest']) and is_alphanum(array_values( array_except($_GET,['cmd']) ), '\-\_') and str_startsWith($_GET['cmd'], 'ffmpeg http://localhost/ch/') ){
			return true;

		# /portal.php?type=stb&action=log&real_action=play&param=[object Object]&content_id=0&tmp_type=1&id=145013&cmd=ffmpeg http://iptvtree.net:8080/oliver/0830415079/145013?play_token=P5i4pqFdFj&JsHttpRequest=1-xml
		} else if( array_keys_in($_GET,['item','del_link','error','subtitles','load','type','action','real_action','param','content_id','tmp_type','id','cmd','JsHttpRequest']) and is_alphanum(array_values( array_except($_GET,['param','cmd']) ), '\-\_') and $_GET['param'] == '[object Object]' and ( str_startsWith($_GET['cmd'], 'ffmpeg http://iptvtree.net:8080/') or str_startsWith($_GET['item'], 'ffmpeg http://iptvtree.net:8080/') ) ){
			return true;

		# /portal.php?type=stb&action=log&real_action=cut_off()&param=&content_id=0&tmp_type=0&JsHttpRequest=1-xml
		} else if( array_keys_in($_GET,['type','action','real_action','param','content_id','tmp_type','JsHttpRequest']) and is_alphanum(array_values( array_except($_GET,['real_action']) ), '\-\_') and $_GET['real_action'] == 'cut_off()' ){
			return true;
		
		# /portal.php?type=stb&action=log&real_action=play&param=ffmpeg%20http%3A%2F%2Fiptvtree.net%3A8080%2Fmoss%2FODdfViHf03%2F1555&content_id=0&tmp_type=1&JsHttpRequest=1-xml
		} else if( array_keys_in($_GET,['type','action','real_action','param','content_id','tmp_type','JsHttpRequest']) and is_alphanum(array_values( array_except($_GET,['param']) ), '\-\_') and str_startsWith($_GET['param'], 'ffmpeg') ){
			return true;
		
		# /portal.php?type=itv&action=get_ordered_list&genre=*&force_ch_link_check=&fav=0&sortby=number&hd=0&p=0&JsHttpRequest=1-xml
		} else if( array_keys_in($_GET,['type','action','genre','force_ch_link_check','fav','sortby','hd','p','JsHttpRequest']) and is_alphanum(array_values( array_except($_GET,['genre']) ), '\-\_') and $_GET['genre'] == '*' ){
			return true;
		}



	}

	// logg(__LINE__, 'bwr');
	logg($uri . ($_POST ? ' POST: '.json_encode($_POST) : '') , 'bwr');

	// return true;
	// logg('non-sei: '.$uri . ($_POST ? ' POST: '.json_encode($_POST) : '') , 'bwr.crazy', $force=true );
	do404('Non sei sicuro di quello che vuoi!');

}






function is_alphanum( $text, $extra='' ){

	if( is_array($text) ){
		
		foreach( $text as $t )
			if(! is_alphanum( $t, $extra ) ) return false;

		return true;

	}

	return $text == mb_ereg_replace('[^A-Za-z0-9'.$extra.']+','',$text);

}



function have_this_keys( $arr, $keys ){
	return sizeof( array_diff( array_keys($arr), $keys ) ) == 0;
}


function array_keys_in( $arr, $valid_keys ){
		
	foreach( array_keys($arr) as $key )
		if(! in_array($key, $valid_keys) ) return false;
	
	return true;

}


function array_except($arr, $keys){
	$new_arr = [];
	foreach( $arr as $k => $v ){
		if(! in_array($k, $keys) ){
			$new_arr[ $k ] = $v;
		}
	}
	return $new_arr;
}


function text_match( $text, $match ){

	$text = "[".$text."]";
	$match = "[".$match."]";

	$p = explode('(*)', $match );

	$pi = 0;
	while( $pi < sizeof($p) ){
		if( $p[$pi+1] and strstr( $text, $p[$pi] ) and strstr( $text, $p[$pi+1] ) ){
			// echo "<hr>".$p[$pi]."<br>".$p[$pi+1]."<hr>";
			$w[$pi] = text_between($text, $p[$pi], $p[$pi+1]);
			$next.= $p[$pi]."(*)";

		} else {
			$next.= $p[$pi];
		}

		$pi++;

	}

	// $next = substr($next, 1, -1);
	// return $next;

	return $next == $match;

}


function get_server_addr( $server_id ){
	include_once("/ram-dir/db/server_addr_by_id.php");
	return $server_addr_array[ $server_id ];
}


function get_serverId_by_streamId( $stream_id ){
	list($code, $rest) = code_n_rest($stream_id);
	include_once("/ram-dir/stream_by_server/".$code.".php");
	return $server_array[ $rest ];
}


function code_n_rest( $the_id ){
	$the_id = number_in_3zero($the_id);
	$code = substr($the_id, -2);
	$rest = substr($the_id, 0, -2);
	return [ $code, $rest ];
}

function number_in_3zero( $numb ){
	switch( strlen($numb) ){
		case 1:  return "00".$numb;
		case 2:  return "0".$numb;
		default: return $numb;
	}
}


function take_care_of_ram_dir( $ram_dir ){
	
	if(! file_exists($ram_dir) ){
		
		shell_exec(" sudo mkdir -p {$ram_dir} ");
		
		if(! file_exists($ram_dir) ){
			logg(__FUNCTION__.": Can't make ram_dir {$ram_dir}", "force", $force=true);
			die;
		}

		shell_exec(" sudo chown -R www-data:www-data {$ram_dir} ");
		shell_exec(" sudo chmod -R 0777 {$ram_dir} ");
		
	}

	return $ram_dir;
}


function take_care_of_file( $file ){
	if(! file_exists($file) ){
		shell_exec("sudo touch $file ; sudo chmod 0777 $file");
	}
	return $file;
}


function block_weird_agent(){

	// file_put_contents('/tmp/'.$_SERVER['REMOTE_ADDR'].'-mag-1-before.json', json_encode($_SERVER));
	// return true;

	foreach( ['HTTP_ACCEPT_LANGUAGE', 'HTTP_ACCEPT_ENCODING', 'HTTP_CONNECTION', 'HTTP_AUTHORIZATION', 'HTTP_X_USER_AGENT'] as $item )
		if(! is_alphanum( str_replace([',',' ','-',';','.','='], '', $_SERVER[$item]) ) )
			unset($_SERVER[$item]);

	$_SERVER['HTTP_ACCEPT'] = 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7';
	
	// if( $_SERVER['HTTP_USER_AGENT'] != '96180ef8e8512f7fd17f84ef1f683c88' )
	// 	$_SERVER['HTTP_USER_AGENT'] = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/112.0.0.0 Safari/537.36';

	$_SERVER['HTTP_UPGRADE_INSECURE_REQUESTS'] = '1';
	$_SERVER['HTTP_CACHE_CONTROL'] = 'max-age=0';

	$port = $_SERVER['SERVER_PORT'];
	if( 
		substr($_SERVER['HTTP_HOST'], -1* (strlen($port)+1) ) != ":{$port}"
		or
		! is_alphanum( trim(str_replace(['.',':'], '', $_SERVER['HTTP_HOST'])) )
	){
		$_SERVER['HTTP_HOST'] = '';
	}

	# HTTP_COOKIE
	# mac=00%3A1A%3A79%3A7A%3A14%3A38; stb_lang=en; timezone=Asia%2FTehran; adid=44606e2d34e94bb7c3c2d1baa216018c
	if( array_key_exists('HTTP_COOKIE', $_SERVER) and !is_alphanum( str_replace([',',' ','-',"=",'%',';','_','.'], '', $_SERVER['HTTP_COOKIE']) ) )
		unset($_SERVER['HTTP_COOKIE']);

	# HTTP_REFERER
	# http://silva.i3ns.net:8080/c/
	if( array_key_exists('HTTP_REFERER', $_SERVER) and !is_alphanum( str_replace(['/','.','-',':'], '', $_SERVER['HTTP_REFERER']) ) )
		unset($_SERVER['HTTP_REFERER']);

	# HTTP_X_REQUESTED_WITH
	if( array_key_exists('HTTP_X_REQUESTED_WITH', $_SERVER) and !is_alphanum( str_replace('.', '', $_SERVER['HTTP_X_REQUESTED_WITH']) ) )
		unset($_SERVER['HTTP_X_REQUESTED_WITH']);

	$http_arr = [ 'HTTP_ACCEPT_LANGUAGE', 'HTTP_ACCEPT_ENCODING', 'HTTP_ACCEPT', 'HTTP_USER_AGENT', 'HTTP_UPGRADE_INSECURE_REQUESTS', 'HTTP_CACHE_CONTROL', 'HTTP_CONNECTION', 'HTTP_HOST', 'HTTP_AUTHORIZATION', 'HTTP_COOKIE', 'HTTP_X_REQUESTED_WITH', 'HTTP_X_USER_AGENT', 'HTTP_REFERER' ];

	foreach( $_SERVER as $k => $v )
		if( substr($k, 0, 4) == 'HTTP' and !in_array($k, $http_arr) )
			unset($_SERVER[ $k ]);

	// file_put_contents('/tmp/'.$_SERVER['REMOTE_ADDR'].'-mag-2-after.json', json_encode($_SERVER));

	// echo "<pre>";
	// var_dump($_SERVER);
	// die;

}



# 2024/11/18
function port( $path, $method='GET', $params=[], $die_on_error=false ){

	$url = PORT_NODE . $path;
	logg(http_build_query($params));
	# POST
	if( strtolower($method) == 'post' ){

		$code = file_get_contents($url, false, stream_context_create([
			'http' => [
				// 'timeout' => $timeout,
				'method'  => 'POST',
				'header'  => 'Content-Type: application/x-www-form-urlencoded',
				'content' => http_build_query($params)
			]
		]));

	# GET
	} else {
		$code = file_get_contents( $url . ( sizeof($params) ? '/?'.http_build_query($params) : '' ) );
	}

    if(! $code ){

		if( $die_on_error ){
			echo is_bool($die_on_error)
				? "can't port the result. {$code}\n"
				: str_replace('%', $code, $die_on_error)."\n";
			die;

		} else {
	        return [ false, "can't port the result. {$code}\n" ];
		}

    } else {

        $json = json_decode($code);
		
		if(! is_object($json) ){
			echo "PORT returned wrong content: \n-----------------------\n{$code}\n-----------------------\n";
			die;

		} else if( array_key_exists('status', (array)$json) and $json->status != "OK" ){
			
			if( $die_on_error ){
				
				echo is_bool($die_on_error)
					? "can't port the result. {$code}\n"
					: str_replace('%', $code, $die_on_error)."\n";

				die;
	
			} else {
	            return [ false, "can't port the result. {$code}\n" ];
			}

		} else {
			
			$data = $json->code;

            return $die_on_error
            	? $data
            	: [ true, $data ]
            	;

        }

    }

}



function json_die( $arr, $skip_encode=false ){

	$json = $skip_encode
		? $arr
		: json_encode($arr)
		;

	if( FLAG_GZIP == true 
		and strstr($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')
		// and ( 0 or text_match($uri, '/panel_api.php?username=(*)&password=(*)') ) 
	){
		$json = gzencode($json, 1);
		header('Content-Encoding: gzip');
	}

	header('Content-Type: application/json');
	header("Pragma: no-cache");
	header("Expires: 0");
	
	echo $json;
	die;

}


